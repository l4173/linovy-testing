package de.sus.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class CalculatorTest {
    @InjectMocks
    Calculator calculator;

    @Test
    void testShouldEqualTwo() {
        // arrange
        final var method = "add";

        // act
        final var add = calculator.calculate("1", "1", method);

        // assert
        assertEquals("2", add);
    }
}
